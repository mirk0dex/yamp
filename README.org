* YAMP
*Yet Another Music Player* (for Android)

I've been looking for a good music player on F-Droid for ages.

Things that I needed were an (AM)OLED pure black theme, playlist & online /.m3u/
support, some Spotify-like suggested music containers in the home screen and a
simple and clean interface. That's basically it. I found no app meeting my
requirements: is that too much to ask for? I do not think so.

So I decided to try and create my very own music player for Android!

This project is young, and still in pre-alpha, this README will be updated soon
for more details. Stay tuned!
